#! /bin/bash

###############################################################################
buildProject() {
  # Make sure the source subdirectories all exist
  /usr/bin/find ../src -type d | sed 's/^..\///' > .directories
  if [[ "CygwinMinGw" == *"${machine}"* ]]; then
    dos2unix .directories
  fi
  for d in $(cat .directories); do mkdir $d -p; done

  # Build the application
  if [ -z $CI ]; then
    parallel="--jobs 16"
  else
    parallel="--jobs 2"
  fi
  make all $parallel
}


# Abort script on errors
set -e

# Figure out if we're linux, mac, or windows
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

# Usage help
if [ "$#" -ne 1 ] ; then
  echo "Usage: $0 [Release/Debug]" >&2
  exit 1
fi

# Change to the correct directory
currentPath="$(pwd -P)"
cd "$(dirname "$0")/../Embedded Project"

# Build the Algorithm library
cd "./Algorithm Source/$1"
buildProject

# Build the Test Firmware and link in the library
cd "../../Algorithm Test Firmware/$1"
buildProject

