# README #

This Atmel Studio Solution consists of 2 projects:

* **Algorithm Source** - Contains a non-functional implementation of the algorithm API.
    * You will need to add code to `algorithm.c` to implement/call the actual algorithm implementation.
    * The output of this project is `libAlgorithm.a`, which will be used in the final product.
* **Algorithm Test** - This project contains a test program for running the algorithm on the SAM4S-EK2 test board.
    * This project may be used to debug the **Algorithm Source** project.
    * This project operates by reading reference database files off of a microSD card and analyzing the data. The results of the analysis may then be reviewed using [a separate set of tools](https://bitbucket.org/tzmedical/ecg-algorithm-test-bench) to determine statistical performance.
    * Output from the board during the test is sent via UART and can be viewed using your preferred COM port viewer.
 
### What do I do first? ###

To start developing and evaluating an algorithm implementation for the Clarus device, you should first fork this repository so you can make changes to your own copy of it.

### Do I need hardware? ###

Yes. This program runs on development boards for the SAM4SD32C microcontroller from Atmel. To run it, you will need to use one of the following:

* An [Atmel SAM4S Xplained Pro](http://www.atmel.com/tools/ATSAM4S-XPRO.aspx) dev board
* A microSD adapter
* And a micro-USB cable

_or_

* An [Atmel SAM4S-EK2](http://www.atmel.com/tools/SAM4S-EK2.aspx) dev board
* An RS232 serial cable
* And an [Atmel SAM-ICE debugger](http://www.atmel.com/tools/ATMELSAM-ICE.aspx)

### How do I get set up on the SAM4S Xplained Pro? ###

Any algorithm used with this firmware must provide the API specified in the `algorithm.h` file in the `Algorithm Source/src` folder. Use the following steps to implement the algorithm and run it.

1. Add any needed source files to the **Algorithm Source** project, and then modify the functions implemented in `algorithm.c`.
1. Prepare a micro SD card with folders for each database to be tested. Each folder should contain databases in the wfdb format (mit, esc, cu, etc). An example file would be located at `E:/mit/100.hea` (If the card was assigned letter E, and record 100 was located in the mit database). *The complete specification for the wfdb database format can be found here: [https://www.physionet.org/physiotools/wag/](https://www.physionet.org/physiotools/wag/)* 
1. Insert the microSD card into an SD Card adapter and insert this into the Atmel SAM4S Xplained Pro development board.
1. Connect the micro-USB cable to the PC and the Xplained Pro.
1. Select the "EDBG" option in the Tools tab of Project Options.
1. Open your preferred COM port viewer and connect to the virtual COM port associated with the Xplained Pro. Set the baud rate to 115200 (8N1).
1. Click run from Atmel Studio.  
1. Monitor the output from the program via the COM port.
1. When the algorithm has finished running, remove the SD card. This completion time can be determined by setting a breakpoint after the call to analyze\_database in the function main in the file `main.c`.

### How do I get set up on the SAM4S-EK2? ###

Any algorithm used with this firmware must provide the API specified in the `algorithm.h` file in the `Algorithm Source/src` folder. Use the following steps to implement the algorithm and run it.

1. Add any needed source files to the **Algorithm Source** project, and then modify the functions implemented in `algorithm.c`.
1. Prepare a micro SD card with folders for each database to be tested. Each folder should contain databases in the wfdb format (mit, esc, cu, etc). An example file would be located at `E:/mit/100.hea` (If the card was assigned letter E, and record 100 was located in the mit database). *The complete specification for the wfdb database format can be found here: [https://www.physionet.org/physiotools/wag/](https://www.physionet.org/physiotools/wag/)* 
1. Insert the microSD card into the Atmel SAM4S-EK2 development board.
1. Connect the power supply cable to the SAM4S-EK2, and then connect the RS232 cable to the "UART" port on the board.
1. Connect the SAM-ICE to the JTAG port on the board, and connect to the PC via USB.
1. Select the "SAM-ICE" option in the Tools tab of Project Options.
1. Open your preferred COM port viewer and connect to the COM port associated with the SAM4S-EK2. Set the baud rate to 115200 (8N1).
1. Click run from Atmel Studio.  
1. Monitor the output from the program via the COM port.
1. When the algorithm has finished running, remove the SD card. This completion time can be determined by setting a breakpoint after the call to analyze\_database in the function main in the file `main.c`.

### How do I review the results? ###

Once the test program has run, a collection of annotation files will be created in each of the database folders. To compare the output annotations with the reference annotations, follow the instructions in [this repository](https://bitbucket.org/tzmedical/ecg-algorithm-test-bench).

