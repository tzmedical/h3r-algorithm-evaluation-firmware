/**
 * \file
 *
 * \brief SD/MMC card example with FatFs
 *
 * Copyright (c) 2012 - 2014 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

#define CONF_USE_UART (1)  // Use UART

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "asf.h"
#include "conf_example.h"
#include <string.h>
#include "algorithm.h"
#include "detection_algorithm.h"
#if CONF_USE_UART == 0
#include "stdio_itm.h"
#else
#include "stdio_serial.h"
#endif

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

const char *ANNOTATION_EXTENSION = (const char[]){"tzm"};

static const char *database_list[] = {
  (const char[]){"mit"},  (const char[]){"esc"}, (const char[]){"nst"},
  (const char[]){"afdb"}, (const char[]){"cu"},
};

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

static Ctrl_status load_sd_card(void);
static void rtc_setup(void);

// Public hook
void vApplicationMallocFailedHook(void);

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

//==============================================================================
// \brief Application entry point.
//
// \return Unused (ANSI-C compatibility).
//==============================================================================
int main(void)
{
  FRESULT fres = FR_OK;
  int32_t res;

  rtc_setup();
  irq_initialize_vectors();
  cpu_irq_enable();

  sysclk_init();
  board_init();

  // Initialize printf
// #define CONF_USE_UART    (0)    // Use SAM_ICE
#if CONF_USE_UART == 0
  stdio_itm_init();
#else
#if BOARD == SAM4S_EK2
  gpio_configure_group(PINS_UART0_PIO, PINS_UART0, PINS_UART0_FLAGS);
  const usart_serial_options_t opts = {
    .baudrate = 115200,
    .paritytype = UART_MR_PAR_NO,
  };
  stdio_serial_init(CONSOLE_UART, &opts);
#elif BOARD == SAM4S_XPLAINED_PRO
  gpio_configure_group(PINS_UART1_PIO, PINS_UART1, PINS_UART1_FLAGS);
  const usart_serial_options_t opts = {
    .baudrate = 115200,
    .paritytype = UART_MR_PAR_NO,
  };
  stdio_serial_init(CONSOLE_UART, &opts);
#else
#error BOARD VALUE INCORRECT!
#endif
#endif

  puts("---------------------------------\r\n");
  puts("TZ Medical Algorithm Test Program\r\n");
  char *p_str = algorithm_get_version_string();
  if ((NULL == p_str) || ('\0' == *p_str)) {
    puts("ERROR: No version string! Aborting.\r\n");
    while (1)
      ;
  }
  printf("Algorithm Version: %s\r\n", p_str);
  puts("---------------------------------\r\n");

  /* Initialize SD MMC stack */
  sd_mmc_init();
  FATFS fs;
  memset(&fs, 0, sizeof(FATFS));
  fres = f_mount(0, &fs);
  if (fres != FR_OK) {
    return ERR_IO_ERROR;
  }

  while (1) {
    delay_ms(1000);

    /* Wait card present and ready */
    load_sd_card();

    uint32_t i;

    for (i = 0; i < sizeof(database_list) / sizeof(const char *); i++) {
      printf("\r\nAnalyzing Database: %s\r\n", database_list[i]);
      res = analyze_database(database_list[i]);
      if (0 != res) {
        // Failure
        printf("--- Failure. Exit code: %d\r\n", (int)res);
      }
      else {
        puts("Complete\r\n");
      }
    }

    f_mount(0, NULL);
    while (1) {
      // Only run the algorithm once
    }
  }

  return 0;
}

//==============================================================================
void vApplicationMallocFailedHook(void)
{
  printf("ERROR: Malloc Failed.\r\n");
  configASSERT(0);
}

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//==============================================================================
static void rtc_setup(void)
{
  pmc_switch_sclk_to_32kxtal(PMC_OSC_XTAL);

  // Wait for oscillator to be ready
  while (!pmc_osc_is_ready_32kxtal())
    ;

  rtc_set_hour_mode(RTC, 0);
}

//==============================================================================
static Ctrl_status load_sd_card(void)
{
  Ctrl_status status;
  printf("Please plug an SD, MMC or SDIO card in slot.\r\n");
  do {
    status = sd_mmc_test_unit_ready(0);
    if (CTRL_FAIL == status) {
      printf("Card install FAIL\r\n");
      printf("Please unplug and re-plug the card.\r\n");
      while (CTRL_NO_PRESENT != sd_mmc_check(0)) {
      }
    }
    // delay_ms(1000);
  } while (CTRL_GOOD != status);
  puts("SD Card Ready.\r\n");
  return status;
}
