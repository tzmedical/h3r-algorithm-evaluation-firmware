//! \file
//! \brief -- THIS FILE NEEDS A BRIEF DESCRIPTION --

#ifndef SPECIAL_WFDB_HEAP_H
#define SPECIAL_WFDB_HEAP_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------
 
 #include "asf.h"
 
//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//! Custom calloc() implementation based on FreeRTOS - Do NOT use in algorithm
void *special_wfdb_calloc( size_t nmemb, size_t size );

//! Custom malloc() implementation based on FreeRTOS - Do NOT use in algorithm
void *special_wfdb_malloc( size_t xWantedSize );

//! Custom free() implementation based on FreeRTOS - Do NOT use in algorithm
void special_wfdb_free( void *pv );

//! Custom realloc() implementation based on FreeRTOS - Do NOT use in algorithm
void *special_wfdb_realloc(void *mem, size_t newsize);

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

#endif /* SPECIAL_WFDB_HEAP_H */