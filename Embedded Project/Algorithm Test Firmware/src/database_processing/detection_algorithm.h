#ifndef DETECTION_ALGORITHM_H
#define DETECTION_ALGORITHM_H

#include <stdint.h>
#include "stdlib.h"
#include "status_codes.h"
#include "wfdb.h"

#ifdef __SAM4SD32C__ // We're on an embedded chip
#define FILE_READ     WFDB_READ
#define FILE_WRITE   WFDB_WRITE
#define FILE_APPEND WFDB_APPEND
#else
#define FILE_READ     "rb"
#define FILE_WRITE    "wb"
#define FILE_APPEND   "ab"
#endif

#define TICK_US (1000)

status_code_t analyze_database(const char *database);

status_code_t get_record_list(char* file_name_base, char*** records, uint32_t* size);


char* my_strdup(const char* s);

#endif // DETECTION_ALGORITHM_H
