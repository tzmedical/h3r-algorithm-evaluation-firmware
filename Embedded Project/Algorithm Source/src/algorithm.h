//! \file
//! \brief This file provides the anticipated API for the ECG analysis algorithm
//! to be used in future TZ Medical embedded designs.

#ifndef ALGORITHM_H
#define ALGORITHM_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <stdint.h>

//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//! This function pointer is used to output beat-level annotations
typedef void (*beat_callback_t) (
        uint8_t beat_label,          //!< The ASCII beat label per 60601-2-47
                                     //!< (i.e. 'N', 'S', 'V', 'F', 'Q')
        int32_t relative_position,  //!< The relative position in samples from the
                                     //!< start of the current analysis block.
        uint16_t current_heart_rate, //!< The current heart rate in Beats Per Minute
        uint8_t channel_mask         //!< A record of which channels detected the beat.
                                     //!< Bit 0 == 1 -> channel 0, etc
        );

//! This function pointer is used to output rhythm-level annotations
typedef void (*rhythm_callback_t)(
        uint8_t rhythm_label,          //!< The ASCII rhythm label (list TBD)
        int32_t relative_position     //!< The relative position in samples from the
                                       //!< start of the current analysis block.
        );

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//! This function will reset/initialize the algorithm for operation. It should
//! be callable multiple times with or without algorithm_close() successfully.
//! (e.g. it shouldn't always be malloc-ing a block of data or something)
//! \return Zero for success, one for failure.
int8_t algorithm_init(
        uint32_t sample_rate_hz,    //!< This is the sample rate of the data to be
                                    //!< analyzed in Hz. Anticipated values are
                                    //!< 250, 500, 1000, 2000, 4000, and 8000
        uint32_t nV_per_count,      //!< This is the digital gain of the signal in
                                    //!< nanoVolts, referred to input (RTI). Typical
                                    //!< values are 1000 to 12000.
        uint8_t number_channels,    //!< The number of channels to be analyzed. Each
                                    //!< call to algorithm_analyze will include this
                                    //!< many channels of data. Typical values are
                                    //!< 1 to 8.
        uint8_t *channel_list       //!< An array identifying the vectors provided
                                    //!< in the ECG channels
        );

//! This function will be called to terminate analysis at the end of a record.
//! It should process any data still being buffered by the algorithm and output
//! the proper annotations. It should then perform any necessary cleanup.
//! \return Zero for success, one for failure.
int8_t algorithm_close(void);

//! This function will register the callback function to handle beat-level
//! algorithm outputs.
//! \return Zero for success, one for failure.
int8_t algorithm_register_beat_callback(
        beat_callback_t callback_function   //!< A function pointer to handle the
                                            //!< beat detections
        );

//! This function will register the callback function to handle rhythm-level
//! algorithm outputs.
//! \return Zero for success, one for failure.
int8_t algorithm_register_rhythm_callback(
        rhythm_callback_t callback_function     //!< A function pointer to handle the
                                                //!< rhythm detections
        );


//! This function can be called at any time to adjust which channels will
//! analyzed. This adjustment continues until the next call to this function or
//! until a subsequent call to algorithm_init().
//!
//! @brief	Set which channels to analyze
//!
//! @param	which		0x01 - analyze channel 1
//! 					0x02 - analyze channel 2
//! 					0x03 - analyze channel 1 and 2
//! 					0x04 - analyze channel 3
//! 						...
//! 					0x07 - analyze channel 1, 2, 3	(default value)
//!
//! @return	void
//!
void algorithm_set_channels_to_analyze(uint8_t channels_to_analyze);

int8_t algorithm_analyze(
        int32_t **p_data,         //!< An array of pointers (number of channels set
                                  //!< in the algorithm_init() function) to the data
                                  //!< to be analyzed.
        uint32_t data_length      //!< The number of samples of data to be analyzed.
        );

//! This function sets the heartrate threshold for determining
//! when tachycardia occurs.
//! \return Zero for success, one for failure.
int8_t algorithm_set_tachycardia_threshold(
        uint16_t threshold_bpm      //!< The new value to be used for determining
                                    //!< tachycardia, in beats per minute.
        );

//! This function sets the heartrate threshold for determining
//! when bradycardia occurs.
//! \return Zero for success, one for failure.
int8_t algorithm_set_bradycardia_threshold(
        uint16_t threshold_bpm      //!< The new value to be used for determining
                                    //!< bradycardia, in beats per minute.
        );

//!< This function sets how many milliseconds different from the Entry Threshold
//!< of Brady and Tachy the Exit Threshold will be
//!< \note  This must be called AFTER algorithm_set_bradycardia_threshold() and
//!< \note  algorithm_set_tachycardia_threshold()
int8_t algorithm_set_rr_variance(
        uint16_t diff_ms            //!< The variance from the thresholds for
                                    //!< brady and tachy before state change
        );

//! This function sets how long of a gap between heartbeats must pass
//! to signify pause.
//! \return Zero for success, one for failure.
int8_t algorithm_set_pause_threshold(
        uint16_t threshold_ms       //!< The new value to be used for determining
                                    //!< pause, in milliseconds.
        );

//! This function sets the minimum time duration for multi-QRS arrhythmias, such
//! as Brady, Tachy, and AF. Pause, PVC, etc. are not affected by this setting.
//! \return Zero for success, one for failure.
int8_t algorithm_set_minimum_duration(
	uint16_t nsr_sec,       //!< The minimum number of seconds an arrhythmia
                          //!< must persist to exit NSR
						              //!< Value range is 0 to 900 seconds.
  uint16_t arrhythmia_sec //!< The minimum number of seconds an arrhythmia must
                          //!< persist to transition from non-NSR rhythms.
						              //!< Value range is 0 to 900 seconds.
);

//! This function returns a pointer to a NULL-terminated string describing the
//! release version of the algorithm. This string should be unique for every
//! version provided to TZ Medical.
//! \return Pointer to the version string
const char * algorithm_get_version_string(void);

#endif /* ALGORITHM_H */


